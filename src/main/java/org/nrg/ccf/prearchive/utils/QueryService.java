/**
 * 
 */
package org.nrg.ccf.prearchive.utils;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.exception.DBPoolException;
import org.nrg.xft.security.UserI;
import org.postgresql.util.PGInterval;

/**
 * The Class QueryService.
 * 
 * @author Atul
 */
public class QueryService {

	/** The Constant GET_ALL_PREARCHIVE_RECORDS_DATA. */
	private static final String GET_ALL_PREARCHIVE_RECORDS_DATA = "get.all.prearchive.records.data";

	/** The Constant GET_ALL_PREARCHIVE_RECORD_COUNTS. */
	private static final String GET_ALL_PREARCHIVE_RECORD_COUNTS = "get.all.prearchive.records.count";

	/** The Constant UPDATE_PREARCHIVE_RECORDS. */
	private static final String UPDATE_PREARCHIVE_RECORDS = "update.prearchive.records";
	
	/** The Constant DEFAULT_HOURS. */
	private static final String DEFAULT_HOURS = "default.hours";

	/** The con. */
	private PoolDBUtils con = new PoolDBUtils();

	/** The get all prearchive records. */
	private PreparedStatement getAllPreArchiveRecordsData = null;

	/** The get all prearchive record counts. */
	private PreparedStatement getAllPreArchiveRecordCounts = null;

	/** The update prearchive records. */
	private PreparedStatement updatePreArchiveRecords = null;

	/**
	 * Gets the all pre archive record counts.
	 *
	 * @return the all pre archive record counts
	 * @throws SQLException
	 *             the SQL exception
	 * @throws DBPoolException
	 *             the DB pool exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Integer getAllPreArchiveRecordCounts(Integer hours) throws SQLException, DBPoolException, IOException {
		ResultSet rs =null;
		int count = 0;
		con = new PoolDBUtils();
		try {
			getAllPreArchiveRecordCounts = con.getPreparedStatement(null,
					PreArchiveUtils.getValue(GET_ALL_PREARCHIVE_RECORD_COUNTS));
			PGInterval pgInterval = new PGInterval();
			pgInterval.setHours(hours != null ? hours : Integer.valueOf(PreArchiveUtils.getValue(DEFAULT_HOURS)));
			getAllPreArchiveRecordCounts.setObject(1, pgInterval);
			rs = getAllPreArchiveRecordCounts.executeQuery();
			if (rs != null) {
				rs.next();
				count = rs.getInt(1);
			}
		}finally {
			closeConnection(rs);
		}
		return count;
	}

	/**
	 * Gets the all pre archive records data as JSON.
	 *
	 * @param user
	 *            the user
	 * @return the all pre archive records data as JSON
	 * @throws SQLException
	 *             the SQL exception
	 * @throws DBPoolException
	 *             the DB pool exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public List<Map<String, Object>> getAllPreArchiveRecordsDataAsJSON(UserI user, Integer hours)
			throws SQLException, DBPoolException, IOException {
		List<Map<String, Object>> listOfMaps = null;
		try {
			con = new PoolDBUtils();
				getAllPreArchiveRecordsData = con.getPreparedStatement(null,
						PreArchiveUtils.getValue(GET_ALL_PREARCHIVE_RECORDS_DATA));

			QueryRunner queryRunner = new QueryRunner();
			listOfMaps = queryRunner.query(getAllPreArchiveRecordsData.getConnection(),
					PreArchiveUtils.getValue(GET_ALL_PREARCHIVE_RECORDS_DATA).replace("?",
							(hours != null ? hours.toString() : PreArchiveUtils.getValue(DEFAULT_HOURS)) + " hours"),
					new MapListHandler());

		} catch (SQLException | DBPoolException | IOException e) {
			e.printStackTrace();
			throw e;
		}
		finally {
			if (getAllPreArchiveRecordsData != null) {
				getAllPreArchiveRecordsData.close();
			}
			closeConnection(null);
		}
		return listOfMaps;
	}

	/**
	 * Update pre archive records.
	 *
	 * @return the integer
	 * @throws SQLException
	 *             the SQL exception
	 * @throws DBPoolException
	 *             the DB pool exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public synchronized Integer updatePreArchiveRecords(Integer hours)
			throws SQLException, DBPoolException, IOException {
		int result=0;
		con = new PoolDBUtils();
		try{
			updatePreArchiveRecords = con.getPreparedStatement(null,
					PreArchiveUtils.getValue(UPDATE_PREARCHIVE_RECORDS));

			PGInterval pgInterval = new PGInterval();
			pgInterval.setHours(hours != null ? hours : Integer.valueOf(PreArchiveUtils.getValue(DEFAULT_HOURS)));
			updatePreArchiveRecords.setObject(1, pgInterval);

			result = updatePreArchiveRecords.executeUpdate();
		}
		finally {
			if (getAllPreArchiveRecordsData != null) {
				getAllPreArchiveRecordsData.close();
			}
			closeConnection(null);
		}
		return result;
	}

	/**
	 * Close connection.
	 *
	 * @param rs the ResultSet Object
	 */
	private void closeConnection(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		con.closeConnection();
	}
}
